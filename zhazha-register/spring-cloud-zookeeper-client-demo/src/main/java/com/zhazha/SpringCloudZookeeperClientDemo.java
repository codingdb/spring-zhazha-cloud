package com.zhazha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Hello world!
 *
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SpringCloudZookeeperClientDemo
{
    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudZookeeperClientDemo.class,args);
    }
}
