package com.zhazha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */
@EnableEurekaClient
@SpringBootApplication
@RestController
public class SpringCloudEurekaClientDemo
{

    @GetMapping("/hello")
    public String hello(){
        return "hello world";
    }
    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudEurekaClientDemo.class,args);
    }
}
