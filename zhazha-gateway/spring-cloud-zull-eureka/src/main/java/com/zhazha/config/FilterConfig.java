package com.zhazha.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName FilterConfig
 * @autor huangpu
 * @DATE 2019/12/17
 **/
@Configuration
public class FilterConfig {


    @Bean
    public TokenFilter tokenFilter(){
        return new TokenFilter();
    }

}

    
    