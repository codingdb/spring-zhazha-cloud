package com.zhazha;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * @ClassName TestController
 * @autor huangpu
 * @DATE 2019/12/21
 **/
@RestController
public class TestController {

    @GetMapping("/order/test")
    public String index(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(auth.toString());
        return "hello world";
    }
}

    
    