package com.zhazha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class SpringCloudOauth2Application
{
    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudOauth2Application.class,args);
    }
}
