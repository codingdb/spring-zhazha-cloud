
这里需要token授权

客户端模式

1. 获取 token 
http://localhost:1001/oauth/token?grant_type=client_credentials&scope=select&client_id=client_1&client_secret=123456

2. 访问资源

http://localhost:1001/order/test?access_token=dd130d63-6f18-4e06-9d86-0289167a70fb1


OAuth 2.0定义了四种授权方式：

1.授权码模式（authorization code）：功能最完整、流程最严密的授权模式。特点是通过第三方应用的后台服务器，与服务提供平台的认证服务器进行互动获取资源。


2.简化模式（implicit）：不通过第三方应用服务器，直接在浏览器中向认证服务器申请token令牌，跳过了授权码这个步骤。所有步骤在浏览器中完成，token对用户可见，且第三方应用不需要认证。


3.密码模式（resource owner password credentials）：用户向第三方应用提供自己的用户名和密码。第三方应用使用这些信息，向服务提供平台索要授权。在这种模式中，用户必须把自己的密码给第三方应用，但是第三方应用不得储存密码。这通常用在用户对第三方应用高度信任的情况下，比如第三方应用是操作系统的一部分，或者由一个著名公司出品。而认证服务器只有在其他授权模式无法执行的情况下，才能考虑使用这种模式。


4.客户端模式（client credentials）：指第三方应用以自己的名义，而不是以用户的名义，向服务提供平台进行认证。严格地说，客户端模式并不属于OAuth框架所要解决的问题。在这种模式中，用户直接向第三方应用注册，第三方应用以自己的名义要求服务提供平台提供服务，其实不存在授权问题。


参考文章
https://blog.csdn.net/ljyhust/article/details/101125390

授权码模式
http://localhost:1001/oauth/authorize?client_id=B5CDC04D8D8D419DA406364168F276A2&response_type=code&redirect_uri=http://localhost:1001/order/test

http://localhost:1001/oauth/authorize?client_id=client1&response_type=code&redirect_uri=http://www.baidu.com


授权码模式获取token
http://localhost:1001/oauth/token?grant_type=authorization_code&code=LfqpDt&redirect_uri=http://www.baidu.com&scope=userProfile&client_id=client1&client_secret=123456


AbstractAuthenticationProcessingFilter
OAuth2AuthenticationProcessingFilter 授权实现
