package com.zhazha.config.server.springcloudconfigserver;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

// 启用config
@EnableConfigServer
@SpringBootApplication

public class SpringCloudConfigServerApplication {

	private  static  Logger logger =  LoggerFactory.getLogger(SpringCloudConfigServerApplication.class);
	public static void main(String[] args) {
		logger.info("开始");
		SpringApplication.run(SpringCloudConfigServerApplication.class, args);
		logger.error("结束");
		logger.info("第三句");
		logger.warn("警告");
	}

}
