package com.zhazha.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @ClassName TaskConfig
 * @autor huangpu
 * @DATE 2020/1/6
 **/
@EnableScheduling
@Component
public class TaskConfig {


    private final ContextRefresher contextRefresher;

    @Autowired
    public TaskConfig(ContextRefresher contextRefresher) {
        this.contextRefresher = contextRefresher;
    }

    //@Scheduled(fixedRate = 5*1000,initialDelay = 3*1000)
    @Scheduled(cron = "0 */1 * * * ?")
    public   void autoRefresh(){
        Set<String> ss=  contextRefresher.refresh();
        ss.forEach(s->{
            System.out.println(s);
        });
    }
}

    
    