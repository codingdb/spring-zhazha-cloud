package com.zhazha.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName GitAutoRefreshCofig
 * @autor huangpu
 * @DATE 2019/12/10
 **/
@Component
@ConfigurationProperties(prefix = "data")
public class GitAutoRefreshCofig {

    private String env;
    private UserInfo user;

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "GitAutoRefreshCofig{" +
                "env='" + env + '\'' +
                ", user=" + user +
                '}';
    }

    public static class UserInfo{
        private String password;
        private String username;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        @Override
        public String toString() {
            return "UserInfo{" +
                    "password='" + password + '\'' +
                    ", username='" + username + '\'' +
                    '}';
        }
    }

}



    
    