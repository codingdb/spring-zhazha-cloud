package com.zhazha;

import com.zhazha.config.GitAutoRefreshCofig;
import com.zhazha.config.GitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName GitController
 * @autor huangpu
 * @DATE 2019/12/10
 * @ResfreshScope
 **/
@RestController
@RefreshScope
public class GitController {


    @Value("${data.env}")
    private String env;


    @Autowired
    private GitConfig gitConfig;

    @Autowired
    private GitAutoRefreshCofig gitAutoRefreshConfig;

    @GetMapping("/env")
    public String env(){
        return env;
    }

    @GetMapping(value = "show")
    public Object show(){
        return gitConfig;
    }

    @GetMapping(value = "autoShow")
    public Object autoShow(){
        return gitAutoRefreshConfig;
    }
}

    
    