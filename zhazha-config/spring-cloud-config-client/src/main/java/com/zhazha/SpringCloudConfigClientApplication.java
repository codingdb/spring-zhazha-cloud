package com.zhazha;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 启动类
 */
@SpringBootApplication

public class SpringCloudConfigClientApplication
{


    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudConfigClientApplication.class,args);
    }



}
