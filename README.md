# spring-zhazha-cloud

#### 介绍

#### 版本

Greenwich SR4
目前构建的版本为Finchley SR4,后期构建完成后改为最新版本

文档:  https://cloud.spring.io/spring-cloud-static/Finchley.SR4/single/spring-cloud.html 

#### 软件架构

软件架构说明


| 模块名称 | 模块说明 |
| ---- | ---- |
|   zhazha-config   | 配置中心  |
| zhazha-gateway | 网关服务 |
| zhazha-load-balancing | 负载均衡模块 |
| zhazha-middle-ware | 中间件操作服务 |
| zhazha-register | 注册中心 |
| zhazha-utils | 全局工具类 |
|      |      |
|      |      |
|      |      |

框架基础
https://github.com/spring-projects/spring-cloud/wiki/Spring-Cloud-Greenwich-Release-Notes
Spring Cloud Greenwich builds on Spring Boot 2.1.x

spring-cloud 版本
Angel
Brixton
Camden
Dalston
Edgware
Finchley 2.0.x
Greenwich  2.1.x
Hoxton  2.2.x  文档没有eureka相关介绍,后期再根据实际情况升级



领域驱动模块

* 领域模块

在一个业务模块中，需要提炼出一个场景，就像拍电影一样，每一个业务模块都受到场景的约束，我们的每一个模块就像拍电影，有它的悲欢离合和业务约束。

组成场景的要素通常被称为6W模型，即描写场景需要who 、what、why 、where、when、how。

领域模块通常很多业务不属于本领域，需要引入限界上下文来解决这个问题。

领域划分谁 -> 浏览商品->下订单->支付

* 统一语言:

统一的领域描述

领域的行为描述

* 限界上下文

我们需要根据业务的相关性、耦合的强弱程度、分离的关注点对这些活动进行归类，找到不同类别之间存在的边界，就是限界上下文。

上下文是业务目标，限界则是保护和隔离上下文的边界，避免业务目标的不单一而带来的混乱和概念的不一致。

微服务

领域  

下订单-上下文 (限界)

数据库-> 服务

限界上下文 api  (划分)

商品 粒度

 

支付



常用的POM属性包括：
 ${project.build.sourceDirectory}:项目的主源码目录，默认为src/main/java/.                            
 ${project.build.testSourceDirectory}:项目的测试源码目录，默认为/src/test/java/.                    
${project.build.directory}:项目构建输出目录，默认为target/.                                                  
 ${project.build.outputDirectory}:项目主代码编译输出目录，默认为target/classes/.                
 ${project.build.testOutputDirectory}:项目测试代码编译输出目录，默认为target/testclasses/.  
${project.groupId}:项目的groupId.                                                                                          
 ${project.artifactId}:项目的artifactId.                                                                                          
${project.version}:项目的version,于${version}等价                                                                    
${project.build.finalName}:项目打包输出文件的名称，默认为  ${project.artifactId}${project.version}.