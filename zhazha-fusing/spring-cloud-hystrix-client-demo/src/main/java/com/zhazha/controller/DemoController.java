package com.zhazha.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName DemoController
 * @autor huangpu
 * @DATE 2020/1/13
 **/
@RestController
public class DemoController {


    @GetMapping("/hello")
    @HystrixCommand(fallbackMethod = "errorBack")
    public String hello(){
        return "hello-world";
    }

    public String errorBack(){
        return "error world";
    }
}

    
    