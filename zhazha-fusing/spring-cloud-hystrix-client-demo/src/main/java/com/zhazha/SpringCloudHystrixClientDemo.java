package com.zhazha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * Hello world!
 *
 */
@SpringBootApplication
//@EnableHystrix
@EnableCircuitBreaker
public class SpringCloudHystrixClientDemo
{
    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudHystrixClientDemo.class,args);
    }
}
