package com.zhazha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * Hello world!
 *http://localhost:7070/hystrix/monitor?stream=http%3A%2F%2Flocalhost%3A8080%2Fhystrix.stream
 */
@SpringBootApplication
@EnableHystrixDashboard
public class SpringCloudHystrixDashboardDemo
{
    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudHystrixDashboardDemo.class,args);
    }
}
