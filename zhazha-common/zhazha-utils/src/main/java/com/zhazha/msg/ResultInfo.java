package com.zhazha.msg;

import java.io.Serializable;

/**
 * @author huangpu
 * @ClassName ResultInfo
 * @autor huangpu
 * @DATE 2019/12/24
 **/
public class ResultInfo implements Serializable {

    private String code;
    private String message;
    private Object data;

    public static ResultInfo success(){
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode("000000");
        return resultInfo;
    }
    public static ResultInfo success(Object data){
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode("000000");
        resultInfo.setData(data);
        return resultInfo;
    }

    public static ResultInfo failure(){
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode("999999");
        return resultInfo;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

    
    