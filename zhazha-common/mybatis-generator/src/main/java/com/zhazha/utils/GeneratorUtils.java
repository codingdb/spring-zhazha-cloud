package com.zhazha.utils;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @ClassName GeneratorUtils
 * @autor huangpu
 * @DATE 2019/12/24
 **/
public  class GeneratorUtils {

    public static  void packagePropertie( Properties propertiesout,String table,String projectPath,Properties propertiesDb){
        propertiesout.put("projectPath",projectPath);
        propertiesout.put("schema",propertiesDb.getProperty("schema"));
        propertiesout.put("base_package",propertiesDb.getProperty("base_package"));
        propertiesout.put("db.url",propertiesDb.getProperty("db.url"));
        propertiesout.put("db.username",propertiesDb.getProperty("db.username"));
        propertiesout.put("db.password",propertiesDb.getProperty("db.password"));
        propertiesout.put("table",table);
        propertiesout.put("tableObject",tableToObject(table));
    }
    public static  void generatorInfo() throws Exception {


        String genCfg=GeneratorUtils.class.getClassLoader().getResource("generatorConfig.xml").getPath();
        boolean overwrite = true;
        List<String> warnings = new ArrayList<String>();
        File configFile = new File(genCfg);
        ConfigurationParser cp = new ConfigurationParser(null);
        Configuration config = null;
        config = cp.parseConfiguration(configFile);
        MyBatisGenerator myBatisGenerator = null;
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        myBatisGenerator = new MyBatisGenerator(config, callback, warnings);

        myBatisGenerator.generate(null);
    }

    public static String tableToObject(String param){
        StringBuilder result=new StringBuilder();
        String a[]=param.split("_");
        for(String s:a){
            result.append(s.substring(0, 1).toUpperCase());
            result.append(s.substring(1).toLowerCase());
        }
        return result.toString();
    }

    public static String objcetToTable(String para){
        StringBuilder sb=new StringBuilder(para);
        int temp=0;//定位
        if (!para.contains("_")) {
            for(int i=0;i<para.length();i++){
                if(Character.isUpperCase(para.charAt(i))){
                    sb.insert(i+temp, "_");
                    temp+=1;
                }
            }
        }
        return sb.toString().toUpperCase();
    }
}

    
    