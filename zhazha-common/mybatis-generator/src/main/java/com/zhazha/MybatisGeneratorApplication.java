package com.zhazha;

import com.zhazha.utils.GeneratorUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class MybatisGeneratorApplication
{


    private static final String settingFile ="base_setting.properties";
    private static final String tempProfile ="db1.properties";

    private static   String  basePath = "";

    private static  final String baseResources = "src/main/resources";

    private static final String baseRoot ="src/main/java";

    public static void main(String[] args)  throws Exception{
        {
            File file = new File("");
            String projectPath = file.getCanonicalPath();
            projectPath = projectPath+ "\\zhazha-common\\mybatis-generator";
            System.out.println(projectPath);
            basePath=GeneratorUtils.class.getClassLoader().getResource("").getPath();

            ///---------------------------获取基础文件---------------------------/
            InputStream inputStream =  MybatisGeneratorApplication.class.getClassLoader().getResourceAsStream(settingFile);
            Properties properties = new Properties();
            properties.load(inputStream);
            String[] tables=properties.getProperty("base_tables").split(",");
            ///---------------------------获取表名，开始循环---------------------------/

            for (String table : tables) {

                String fullPath = basePath+"/"+tempProfile;
                FileOutputStream out = new FileOutputStream(fullPath);
                Properties propertiesout = new Properties();
                GeneratorUtils.packagePropertie(propertiesout,table,projectPath,properties);
                propertiesout.store(out,"auto generator message");

                ///---------------------------开始生成---------------------------/
                GeneratorUtils.generatorInfo();
                System.out.println(table+"处理完成");
            }

        }


    }
}
