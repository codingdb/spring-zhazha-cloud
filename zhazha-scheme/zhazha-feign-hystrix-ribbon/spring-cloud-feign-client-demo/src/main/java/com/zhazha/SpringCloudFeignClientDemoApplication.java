package com.zhazha;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName SpringCloudFeignClientDemoApplication
 * @autor huangpu
 * @DATE 2020/1/16
 **/
@SpringBootApplication
@EnableFeignClients
public class SpringCloudFeignClientDemoApplication {
}

    
    